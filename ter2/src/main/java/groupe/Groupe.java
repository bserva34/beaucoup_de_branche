package groupe;
import java.util.*;
import java.io.Serializable;

public class Groupe implements Serializable  {
	private int id;
	private String nom;
	private ArrayList<Sujet> voeux = new ArrayList<>();
	
	public Groupe(int id, String nom) {
		this.id = id;
		this.nom = nom;
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public ArrayList<Sujet> getVoeux() {
		return voeux;
	}
	public void setVoeux(ArrayList<Sujet> voeux) {
		this.voeux = voeux;
	}
	@Override
	public String toString() {
		return "Groupe [id=" + id + ", nom=" + nom + ", voeux=" + voeux + "]";
	}
	public void ajouter(Sujet e) {
		voeux.add(e);
		e.setId_groupe(this.id);
	}
	
	
	
	
	
	

}
