package fr.umfds.ter2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import groupe.Groupe;
import groupe.Sujet;

public class Test2 {
	Groupe g1 = new Groupe(1,"ABAM");
	Groupe g2 = new Groupe(2,"TEST");
	Groupe g3 = new Groupe(3,"SPORT");
	Sujet s1 = new Sujet(1,"Sport");
	Sujet s2 = new Sujet(2,"Livre");
	Sujet s3 = new Sujet(3,"Art");
	
	@BeforeEach
	public void init() {
		g1.ajouter(s2);
		g2.ajouter(s3);
		g3.ajouter(s1);
	}
	
	@Test 
	public void test_affectation() {
		assertEquals(1,s2.getId_groupe());
	}
	

	
}

